package com.example.spring_deploy_viktor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDeployViktorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDeployViktorApplication.class, args);
	}

}
